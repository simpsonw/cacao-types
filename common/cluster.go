package common

// Cluster ...
type Cluster struct {
	ID               string `json:"id,omitempty"`
	Name             string `json:"name,omitempty"`
	DefaultNamespace string `json:"default_namespace,omitempty"`
	Config           string `json:"config,omitempty"`
	Host             string `json:"host,omitempty"`
	// Slug is a seed for cluster resource naming.
	// Use the Slug value for naming if the name is not occupied.
	// Otherwise, use it as a prefix.
	Slug  string `json:"slug,omitempty"`
	Admin bool   `json:"admin,omitempty"`
}

// GetRedacted will return a new Cluster with the Value set to "REDACTED"
func (c *Cluster) GetRedacted() Cluster {
	redactedCluster := *c
	redactedCluster.Config = "REDACTED"
	return redactedCluster
}
