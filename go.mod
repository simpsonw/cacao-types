module gitlab.com/cyverse/cacao-types

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/cloudevents/sdk-go/v2 v2.3.1 // indirect
	github.com/nats-io/nats.go v1.10.0 // indirect
	github.com/nats-io/stan.go v0.8.2 // indirect
	github.com/prometheus/common v0.0.0-20181126121408-4724e9255275 // indirect
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.8.1 // indirect
	gitlab.com/cyverse/cacao-utilities v0.0.0-20210409210212-3ac73039e46b // indirect
	golang.org/x/tools v0.0.0-20210106214847-113979e3529a // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6 // indirect
)
